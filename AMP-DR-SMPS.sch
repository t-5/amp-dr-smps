EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "AMP-DR (PSU)"
Date "2021-05-31"
Rev "3.0"
Comp "T5! DIY Audio"
Comment1 "HttPS://t-5.eu/hp/AMP-DR"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SamacSys_Parts:VS-3EMU06 D1
U 1 1 5FC306D1
P 6900 1500
AR Path="/5FC306D1" Ref="D1"  Part="1" 
AR Path="/5FC0DE84/5FC306D1" Ref="D?"  Part="1" 
F 0 "D1" V 7104 1588 50  0000 L CNN
F 1 "STPS3L40" V 7195 1588 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 7350 1500 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 7350 1400 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 7350 1300 50  0001 L CNN "Description"
F 5 "2.29" H 7350 1200 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 7350 1100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 7350 1000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 7350 900 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 7350 800 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 2950 8250 2950
Wire Wire Line
	7550 4050 8250 4050
Connection ~ 8250 4050
Connection ~ 8250 2950
Wire Wire Line
	7550 1500 8250 1500
Wire Wire Line
	7550 2600 8250 2600
Connection ~ 8250 2600
Connection ~ 8250 1500
Wire Wire Line
	6900 1500 6900 1600
Wire Wire Line
	6900 1950 6900 1900
Connection ~ 7550 1500
Wire Wire Line
	7550 1600 7550 1500
Wire Wire Line
	6900 1500 7550 1500
Wire Wire Line
	7550 2050 7550 1900
Wire Wire Line
	7550 2150 7550 2200
Wire Wire Line
	7200 2150 7550 2150
Wire Wire Line
	7200 1950 7200 2150
Wire Wire Line
	6900 1950 7200 1950
Wire Wire Line
	7550 2050 6900 2050
Wire Wire Line
	6900 2500 6900 2600
Wire Wire Line
	7550 2600 7550 2500
Wire Wire Line
	8750 2200 8750 2600
Wire Wire Line
	8750 1500 8750 1900
$Comp
L Device:CP C2
U 1 1 5FC3070C
P 8750 2050
AR Path="/5FC3070C" Ref="C2"  Part="1" 
AR Path="/5FC0DE84/5FC3070C" Ref="C?"  Part="1" 
F 0 "C2" H 8868 2096 50  0000 L CNN
F 1 "5600u" H 8868 2005 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 8788 1900 50  0001 C CNN
F 3 "~" H 8750 2050 50  0001 C CNN
	1    8750 2050
	1    0    0    -1  
$EndComp
Connection ~ 7550 2600
Wire Wire Line
	8250 2600 8250 2200
Wire Wire Line
	8250 1500 8250 1900
$Comp
L Device:CP C1
U 1 1 5FC306FC
P 8250 2050
AR Path="/5FC306FC" Ref="C1"  Part="1" 
AR Path="/5FC0DE84/5FC306FC" Ref="C?"  Part="1" 
F 0 "C1" H 8368 2096 50  0000 L CNN
F 1 "5600u" H 8368 2005 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 8288 1900 50  0001 C CNN
F 3 "~" H 8250 2050 50  0001 C CNN
	1    8250 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2600 7550 2600
$Comp
L SamacSys_Parts:VS-3EMU06 D4
U 1 1 5FC306F5
P 7550 2100
AR Path="/5FC306F5" Ref="D4"  Part="1" 
AR Path="/5FC0DE84/5FC306F5" Ref="D?"  Part="1" 
F 0 "D4" V 7754 2188 50  0000 L CNN
F 1 "STPS3L40" V 7845 2188 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 8000 2100 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 8000 2000 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 8000 1900 50  0001 L CNN "Description"
F 5 "2.29" H 8000 1800 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 8000 1700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 8000 1600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 8000 1500 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 8000 1400 50  0001 L CNN "Manufacturer_Part_Number"
	1    7550 2100
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:VS-3EMU06 D2
U 1 1 5FC306E9
P 7550 1500
AR Path="/5FC306E9" Ref="D2"  Part="1" 
AR Path="/5FC0DE84/5FC306E9" Ref="D?"  Part="1" 
F 0 "D2" V 7754 1588 50  0000 L CNN
F 1 "STPS3L40" V 7845 1588 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 8000 1500 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 8000 1400 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 8000 1300 50  0001 L CNN "Description"
F 5 "2.29" H 8000 1200 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 8000 1100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 8000 1000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 8000 900 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 8000 800 50  0001 L CNN "Manufacturer_Part_Number"
	1    7550 1500
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:VS-3EMU06 D3
U 1 1 5FC306DD
P 6900 2100
AR Path="/5FC306DD" Ref="D3"  Part="1" 
AR Path="/5FC0DE84/5FC306DD" Ref="D?"  Part="1" 
F 0 "D3" V 7104 2188 50  0000 L CNN
F 1 "STPS3L40" V 7195 2188 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 7350 2100 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 7350 2000 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 7350 1900 50  0001 L CNN "Description"
F 5 "2.29" H 7350 1800 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 7350 1700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 7350 1600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 7350 1500 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 7350 1400 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 2950 6900 3050
Wire Wire Line
	6900 3400 6900 3350
Connection ~ 7550 2950
Wire Wire Line
	7550 3050 7550 2950
Wire Wire Line
	6900 2950 7550 2950
Wire Wire Line
	7550 3500 7550 3350
Wire Wire Line
	7550 3600 7550 3650
Wire Wire Line
	7200 3600 7550 3600
Wire Wire Line
	7200 3400 7200 3600
Wire Wire Line
	6900 3400 7200 3400
Wire Wire Line
	7550 3500 6900 3500
Wire Wire Line
	6900 3950 6900 4050
Wire Wire Line
	7550 4050 7550 3950
Wire Wire Line
	8750 3650 8750 4050
Wire Wire Line
	8750 2950 8750 3350
$Comp
L Device:CP C11
U 1 1 5FC30668
P 8750 3500
AR Path="/5FC30668" Ref="C11"  Part="1" 
AR Path="/5FC0DE84/5FC30668" Ref="C?"  Part="1" 
F 0 "C11" H 8868 3546 50  0000 L CNN
F 1 "5600u" H 8868 3455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 8788 3350 50  0001 C CNN
F 3 "~" H 8750 3500 50  0001 C CNN
	1    8750 3500
	1    0    0    -1  
$EndComp
Connection ~ 7550 4050
Wire Wire Line
	8250 4050 8250 3650
Wire Wire Line
	8250 2950 8250 3350
$Comp
L Device:CP C10
U 1 1 5FC305DB
P 8250 3500
AR Path="/5FC305DB" Ref="C10"  Part="1" 
AR Path="/5FC0DE84/5FC305DB" Ref="C?"  Part="1" 
F 0 "C10" H 8368 3546 50  0000 L CNN
F 1 "5600u" H 8368 3455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 8288 3350 50  0001 C CNN
F 3 "~" H 8250 3500 50  0001 C CNN
	1    8250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4050 7550 4050
$Comp
L SamacSys_Parts:VS-3EMU06 D8
U 1 1 5FC305D4
P 7550 3550
AR Path="/5FC305D4" Ref="D8"  Part="1" 
AR Path="/5FC0DE84/5FC305D4" Ref="D?"  Part="1" 
F 0 "D8" V 7754 3638 50  0000 L CNN
F 1 "STPS3L40" V 7845 3638 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 8000 3550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 8000 3450 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 8000 3350 50  0001 L CNN "Description"
F 5 "2.29" H 8000 3250 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 8000 3150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 8000 3050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 8000 2950 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 8000 2850 50  0001 L CNN "Manufacturer_Part_Number"
	1    7550 3550
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:VS-3EMU06 D6
U 1 1 5FC305C8
P 7550 2950
AR Path="/5FC305C8" Ref="D6"  Part="1" 
AR Path="/5FC0DE84/5FC305C8" Ref="D?"  Part="1" 
F 0 "D6" V 7754 3038 50  0000 L CNN
F 1 "STPS3L40" V 7845 3038 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 8000 2950 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 8000 2850 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 8000 2750 50  0001 L CNN "Description"
F 5 "2.29" H 8000 2650 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 8000 2550 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 8000 2450 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 8000 2350 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 8000 2250 50  0001 L CNN "Manufacturer_Part_Number"
	1    7550 2950
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:VS-3EMU06 D7
U 1 1 5FC305BC
P 6900 3550
AR Path="/5FC305BC" Ref="D7"  Part="1" 
AR Path="/5FC0DE84/5FC305BC" Ref="D?"  Part="1" 
F 0 "D7" V 7104 3638 50  0000 L CNN
F 1 "STPS3L40" V 7195 3638 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 7350 3550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 7350 3450 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 7350 3350 50  0001 L CNN "Description"
F 5 "2.29" H 7350 3250 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 7350 3150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 7350 3050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 7350 2950 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 7350 2850 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 3550
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:VS-3EMU06 D5
U 1 1 5FC305B0
P 6900 2950
AR Path="/5FC305B0" Ref="D5"  Part="1" 
AR Path="/5FC0DE84/5FC305B0" Ref="D?"  Part="1" 
F 0 "D5" V 7104 3038 50  0000 L CNN
F 1 "STPS3L40" V 7195 3038 50  0000 L CNN
F 2 "SamacSys_Parts:DIOM8059X265N" H 7350 2950 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/VS-3EMU06-M3_5AT.pdf" H 7350 2850 50  0001 L CNN
F 4 "Rectifiers 3 Amp 600 Volts Ultrafast - Low VF" H 7350 2750 50  0001 L CNN "Description"
F 5 "2.29" H 7350 2650 50  0001 L CNN "Height"
F 6 "78-VS-3EMU06-M3/5AT" H 7350 2550 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=78-VS-3EMU06-M3%2F5AT" H 7350 2450 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 7350 2350 50  0001 L CNN "Manufacturer_Name"
F 9 "VS-3EMU06-M3/5AT" H 7350 2250 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 3600 5850 3600
Wire Wire Line
	6650 3400 6900 3400
Wire Wire Line
	5700 1950 5850 1950
Wire Wire Line
	6650 2150 6900 2150
$Comp
L ose-audio-labs:TRANSFO_DUAL-DUAL T1
U 1 1 5FCF655B
P 5200 2800
F 0 "T1" H 5200 3500 50  0000 C CNN
F 1 "Talema 70083K" H 5200 3400 50  0000 C CNN
F 2 "T5_w11:Talema_70083K_50VA" H 5200 3050 50  0001 C CNN
F 3 "" H 5200 3050 50  0000 C CNN
	1    5200 2800
	1    0    0    1   
$EndComp
Wire Wire Line
	5700 1950 5700 2300
Wire Wire Line
	5700 2300 5600 2300
Wire Wire Line
	6650 2150 6650 2700
Wire Wire Line
	6650 2700 6250 2700
Wire Wire Line
	5600 2850 5850 2850
Wire Wire Line
	6650 2850 6650 3400
Wire Wire Line
	5600 3250 5700 3250
Wire Wire Line
	5700 3250 5700 3600
Wire Wire Line
	4800 2700 4750 2700
Wire Wire Line
	4750 2700 4750 2850
Wire Wire Line
	4750 2850 4800 2850
Connection ~ 9250 2600
Connection ~ 9250 1500
Wire Wire Line
	9250 2600 9250 2200
Wire Wire Line
	9250 1500 9250 1900
$Comp
L Device:CP C3
U 1 1 5FD21B8E
P 9250 2050
AR Path="/5FD21B8E" Ref="C3"  Part="1" 
AR Path="/5FC0DE84/5FD21B8E" Ref="C?"  Part="1" 
F 0 "C3" H 9368 2096 50  0000 L CNN
F 1 "5600u" H 9368 2005 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 9288 1900 50  0001 C CNN
F 3 "~" H 9250 2050 50  0001 C CNN
	1    9250 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2600 8750 2600
Wire Wire Line
	8250 1500 8750 1500
Connection ~ 9250 4050
Connection ~ 9250 2950
Wire Wire Line
	9250 4050 9250 3650
Wire Wire Line
	9250 2950 9250 3350
$Comp
L Device:CP C12
U 1 1 5FD23FCE
P 9250 3500
AR Path="/5FD23FCE" Ref="C12"  Part="1" 
AR Path="/5FC0DE84/5FD23FCE" Ref="C?"  Part="1" 
F 0 "C12" H 9368 3546 50  0000 L CNN
F 1 "5600u" H 9368 3455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 9288 3350 50  0001 C CNN
F 3 "~" H 9250 3500 50  0001 C CNN
	1    9250 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4050 8750 4050
Wire Wire Line
	8250 2950 8750 2950
Connection ~ 8750 1500
Wire Wire Line
	8750 1500 9250 1500
Connection ~ 8750 2600
Wire Wire Line
	8750 2600 9250 2600
Connection ~ 8750 2950
Wire Wire Line
	8750 2950 9250 2950
Connection ~ 8750 4050
Wire Wire Line
	8750 4050 9250 4050
$Comp
L Device:EMI_Filter_CommonMode FL1
U 1 1 5FD485DC
P 2600 2750
F 0 "FL1" H 2600 3031 50  0000 C CNN
F 1 "10mH" H 2600 2940 50  0000 C CNN
F 2 "SamacSys_Parts:RN218140210M" H 2600 2790 50  0001 C CNN
F 3 "~" H 2600 2790 50  0001 C CNN
	1    2600 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:EMI_Filter_CommonMode FL2
U 1 1 5FD49A11
P 4000 2750
F 0 "FL2" H 4000 3031 50  0000 C CNN
F 1 "10mH" H 4000 2940 50  0000 C CNN
F 2 "SamacSys_Parts:RN218140210M" H 4000 2790 50  0001 C CNN
F 3 "~" H 4000 2790 50  0001 C CNN
	1    4000 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5FD4A18A
P 3450 2750
F 0 "C7" H 3565 2796 50  0000 L CNN
F 1 "100n" H 3565 2705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L18.0mm_W5.0mm_P15.00mm_FKS3_FKP3" H 3488 2600 50  0001 C CNN
F 3 "~" H 3450 2750 50  0001 C CNN
	1    3450 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2650 2800 2300
Wire Wire Line
	2800 2300 3150 2300
Wire Wire Line
	3450 2300 3450 2600
Wire Wire Line
	3800 2300 3800 2650
Connection ~ 3450 2300
Wire Wire Line
	2800 2850 2800 3250
Wire Wire Line
	2800 3250 3150 3250
Wire Wire Line
	3450 3250 3450 2900
Wire Wire Line
	3800 3250 3800 2850
Connection ~ 3450 3250
$Comp
L Device:C C8
U 1 1 5FD562AD
P 4400 2750
F 0 "C8" H 4515 2796 50  0000 L CNN
F 1 "100n" H 4515 2705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L18.0mm_W5.0mm_P15.00mm_FKS3_FKP3" H 4438 2600 50  0001 C CNN
F 3 "~" H 4400 2750 50  0001 C CNN
	1    4400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2650 4200 2300
Wire Wire Line
	4200 2300 4400 2300
Wire Wire Line
	4400 2300 4400 2600
Connection ~ 4400 2300
Wire Wire Line
	4200 2850 4200 3250
Wire Wire Line
	4200 3250 4400 3250
Wire Wire Line
	4400 3250 4400 2900
Connection ~ 4400 3250
$Comp
L Device:C C6
U 1 1 5FD76272
P 2050 2750
F 0 "C6" H 2165 2796 50  0000 L CNN
F 1 "100n" H 2165 2705 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L18.0mm_W5.0mm_P15.00mm_FKS3_FKP3" H 2088 2600 50  0001 C CNN
F 3 "~" H 2050 2750 50  0001 C CNN
	1    2050 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2300 2050 2600
Wire Wire Line
	2400 2300 2400 2650
Wire Wire Line
	2050 2300 2400 2300
Wire Wire Line
	2400 2850 2400 3250
Wire Wire Line
	2050 3250 2400 3250
Wire Wire Line
	3450 2300 3800 2300
Wire Wire Line
	3450 3250 3800 3250
Wire Wire Line
	4400 2300 4800 2300
Wire Wire Line
	4400 3250 4800 3250
Wire Wire Line
	9700 1500 9700 2000
Wire Wire Line
	9700 2100 9700 2600
Wire Wire Line
	9700 2950 9700 3450
Wire Wire Line
	9700 4050 9700 3550
$Comp
L Device:C C9
U 1 1 5FCE7DD3
P 3150 3050
F 0 "C9" H 3265 3096 50  0000 L CNN
F 1 "1n" H 3265 3005 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L10.0mm_W4.0mm_P7.50mm_MKS4" H 3188 2900 50  0001 C CNN
F 3 "~" H 3150 3050 50  0001 C CNN
	1    3150 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2350 3150 2300
Wire Wire Line
	3150 3200 3150 3250
Wire Wire Line
	950  2750 850  2750
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FE58EA1
P 1400 5850
F 0 "H1" V 1600 5750 50  0000 C CNN
F 1 "MountingHole_Pad" V 1500 5450 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1400 5850 50  0001 C CNN
F 3 "~" H 1400 5850 50  0001 C CNN
	1    1400 5850
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FE5A04A
P 1350 6050
F 0 "H2" H 1450 6096 50  0000 L CNN
F 1 "MountingHole" H 1450 6005 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1350 6050 50  0001 C CNN
F 3 "~" H 1350 6050 50  0001 C CNN
	1    1350 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FE5A400
P 1350 6250
F 0 "H3" H 1450 6296 50  0000 L CNN
F 1 "MountingHole" H 1450 6205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1350 6250 50  0001 C CNN
F 3 "~" H 1350 6250 50  0001 C CNN
	1    1350 6250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FE5A57C
P 1350 6450
F 0 "H4" H 1450 6496 50  0000 L CNN
F 1 "MountingHole" H 1450 6405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1350 6450 50  0001 C CNN
F 3 "~" H 1350 6450 50  0001 C CNN
	1    1350 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5FE604C0
P 1600 5850
F 0 "#PWR0101" H 1600 5600 50  0001 C CNN
F 1 "GND" V 1605 5722 50  0000 R CNN
F 2 "" H 1600 5850 50  0001 C CNN
F 3 "" H 1600 5850 50  0001 C CNN
	1    1600 5850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 5850 1500 5850
Wire Wire Line
	3150 2650 3150 2700
Connection ~ 3150 2300
Wire Wire Line
	3150 2300 3450 2300
Connection ~ 3150 3250
Wire Wire Line
	3150 3250 3450 3250
$Comp
L power:GND #PWR02
U 1 1 5FD421B8
P 2950 2750
F 0 "#PWR02" H 2950 2500 50  0001 C CNN
F 1 "GND" H 2955 2577 50  0000 C CNN
F 2 "" H 2950 2750 50  0001 C CNN
F 3 "" H 2950 2750 50  0001 C CNN
	1    2950 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2750 2950 2700
Wire Wire Line
	2950 2700 3150 2700
Connection ~ 3150 2700
Wire Wire Line
	3150 2700 3150 2900
$Comp
L Device:C C5
U 1 1 5FCE3179
P 3150 2500
F 0 "C5" H 3265 2546 50  0000 L CNN
F 1 "1n" H 3265 2455 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L10.0mm_W4.0mm_P7.50mm_MKS4" H 3188 2350 50  0001 C CNN
F 3 "~" H 3150 2500 50  0001 C CNN
	1    3150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3250 2050 2900
Connection ~ 2050 3250
Wire Wire Line
	9250 1500 9700 1500
Wire Wire Line
	9250 2600 9700 2600
Wire Wire Line
	9250 2950 9700 2950
Wire Wire Line
	9250 4050 9700 4050
$Comp
L t-5:T5 e1
U 1 1 5FDA582E
P 1700 6950
F 0 "e1" H 1700 7050 50  0001 C CNN
F 1 "T5" H 1700 6950 50  0001 C CNN
F 2 "T5_w11:T5_w11" H 1700 6950 50  0001 C CNN
F 3 "" H 1700 6950 50  0001 C CNN
	1    1700 6950
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:Molex_43045-0425 J1
U 1 1 601744E0
P 9750 2850
F 0 "J1" H 10378 2971 50  0000 L CNN
F 1 "Molex_43045-0425" H 10378 2880 50  0000 L CNN
F 2 "SamacSys_Parts:430450425" H 10600 3150 50  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/430452227_sd.pdf" H 10600 3050 50  0001 L CNN
F 4 "Micro-Fit 3.0 Vertical Header, 3.00mm Pitch, Dual Row,  Circuits, with PCB Polarizing Peg, Tin, Glow-Wire Capable, Black" H 10600 2950 50  0001 L CNN "Description"
F 5 "10.16" H 10600 2850 50  0001 L CNN "Height"
F 6 "538-43045-0425" H 10600 2750 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/43045-0425?qs=jbJnFeK%2FK6zj9cttOVFa9Q%3D%3D" H 10600 2650 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 10600 2550 50  0001 L CNN "Manufacturer_Name"
F 9 "43045-0425" H 10600 2450 50  0001 L CNN "Manufacturer_Part_Number"
	1    9750 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 2100 10250 2350
Wire Wire Line
	9700 2100 10250 2100
Wire Wire Line
	10150 2000 10150 2350
Wire Wire Line
	9700 2000 10150 2000
Wire Wire Line
	10150 3450 10150 3200
Wire Wire Line
	9700 3450 10150 3450
Wire Wire Line
	10250 3550 10250 3200
Wire Wire Line
	9700 3550 10250 3550
NoConn ~ 10050 2350
NoConn ~ 10050 3200
Wire Wire Line
	6900 2050 6900 2150
Wire Wire Line
	6900 3500 6900 3600
$Comp
L Device:R R1
U 1 1 601F8685
P 5850 2150
F 0 "R1" H 5920 2196 50  0000 L CNN
F 1 "38R5" H 5920 2105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5780 2150 50  0001 C CNN
F 3 "~" H 5850 2150 50  0001 C CNN
	1    5850 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 601F9F22
P 5850 2500
F 0 "C13" H 5965 2546 50  0000 L CNN
F 1 "150n" H 5965 2455 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L13.0mm_W4.0mm_P10.00mm_FKS3_FKP3_MKS4" H 5888 2350 50  0001 C CNN
F 3 "~" H 5850 2500 50  0001 C CNN
	1    5850 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 601FA5CA
P 6250 2350
F 0 "C4" H 6365 2396 50  0000 L CNN
F 1 "10n" H 6365 2305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L13.0mm_W4.0mm_P10.00mm_FKS3_FKP3_MKS4" H 6288 2200 50  0001 C CNN
F 3 "~" H 6250 2350 50  0001 C CNN
	1    6250 2350
	1    0    0    -1  
$EndComp
Connection ~ 6900 1950
Connection ~ 6900 2150
Wire Wire Line
	6900 2150 6900 2200
Wire Wire Line
	5850 2300 5850 2350
$Comp
L Device:R R2
U 1 1 602248AB
P 5850 3050
F 0 "R2" H 5920 3096 50  0000 L CNN
F 1 "38R5" H 5920 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5780 3050 50  0001 C CNN
F 3 "~" H 5850 3050 50  0001 C CNN
	1    5850 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C15
U 1 1 602248B5
P 5850 3400
F 0 "C15" H 5965 3446 50  0000 L CNN
F 1 "150n" H 5965 3355 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L13.0mm_W4.0mm_P10.00mm_FKS3_FKP3_MKS4" H 5888 3250 50  0001 C CNN
F 3 "~" H 5850 3400 50  0001 C CNN
	1    5850 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 602248BF
P 6250 3250
F 0 "C14" H 6365 3296 50  0000 L CNN
F 1 "10n" H 6365 3205 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L13.0mm_W4.0mm_P10.00mm_FKS3_FKP3_MKS4" H 6288 3100 50  0001 C CNN
F 3 "~" H 6250 3250 50  0001 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3200 5850 3250
Connection ~ 6900 3400
Connection ~ 6900 3600
Wire Wire Line
	6900 3600 6900 3650
Wire Wire Line
	5850 2900 5850 2850
Connection ~ 5850 2850
Wire Wire Line
	5850 2850 6250 2850
Wire Wire Line
	6250 3100 6250 2850
Connection ~ 6250 2850
Wire Wire Line
	6250 2850 6650 2850
Wire Wire Line
	6250 3400 6250 3600
Connection ~ 6250 3600
Wire Wire Line
	6250 3600 6900 3600
Wire Wire Line
	5850 3550 5850 3600
Connection ~ 5850 3600
Wire Wire Line
	5850 3600 6250 3600
Wire Wire Line
	5850 2000 5850 1950
Connection ~ 5850 1950
Wire Wire Line
	5850 1950 6250 1950
Wire Wire Line
	6250 1950 6250 2200
Connection ~ 6250 1950
Wire Wire Line
	6250 1950 6900 1950
Wire Wire Line
	6250 2500 6250 2700
Connection ~ 6250 2700
Wire Wire Line
	6250 2700 5850 2700
Wire Wire Line
	5850 2650 5850 2700
Connection ~ 5850 2700
Wire Wire Line
	5850 2700 5600 2700
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 60597D0A
P 650 2650
F 0 "J2" H 758 2831 50  0000 C CNN
F 1 "AC" H 750 2750 50  0000 C CNN
F 2 "T5_w11:Flatpin_4mm" H 650 2650 50  0001 C CNN
F 3 "~" H 650 2650 50  0001 C CNN
	1    650  2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 6059879B
P 650 2750
F 0 "J3" H 750 2600 50  0000 C CNN
F 1 "AC" H 750 2700 50  0000 C CNN
F 2 "T5_w11:Flatpin_4mm" H 650 2750 50  0001 C CNN
F 3 "~" H 650 2750 50  0001 C CNN
	1    650  2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2650 1350 2550
Connection ~ 2050 2300
Wire Wire Line
	1250 2300 2050 2300
Wire Wire Line
	850  2650 1350 2650
Wire Wire Line
	1350 3000 1350 3100
Wire Wire Line
	1250 2300 1250 3350
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 60599002
P 1250 3550
F 0 "J4" H 1150 3550 50  0000 C CNN
F 1 "SW1" H 950 3550 50  0000 C CNN
F 2 "T5_w11:Flatpin_4mm" H 1250 3550 50  0001 C CNN
F 3 "~" H 1250 3550 50  0001 C CNN
	1    1250 3550
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 6059A5A7
P 1350 3550
F 0 "J5" H 1250 3550 50  0000 C CNN
F 1 "SW1" H 1050 3550 50  0000 C CNN
F 2 "T5_w11:Flatpin_4mm" H 1350 3550 50  0001 C CNN
F 3 "~" H 1350 3550 50  0001 C CNN
	1    1350 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	950  2750 950  3250
Wire Wire Line
	950  3250 2050 3250
$Comp
L SamacSys_Parts:0751.0062 F101
U 1 1 60B7DB16
P 1350 2550
F 0 "F101" V 1700 2500 50  0000 L CNN
F 1 "0751.0062" V 1800 2350 50  0000 L CNN
F 2 "07510062" H 2000 2650 50  0001 L CNN
F 3 "https://us.schurter.com/en/datasheet/typ_OG__Clip__5x20.pdf" H 2000 2550 50  0001 L CNN
F 4 "Fuse Clip 6.3A 500VDC/500VAC Solder Pin Through Hole Bulk" H 2000 2450 50  0001 L CNN "Description"
F 5 "12" H 2000 2350 50  0001 L CNN "Height"
F 6 "693-0751.0062" H 2000 2250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Schurter/07510062?qs=WtG364jHAdwzmGym3exHtQ%3D%3D" H 2000 2150 50  0001 L CNN "Mouser Price/Stock"
F 8 "SCHURTER" H 2000 2050 50  0001 L CNN "Manufacturer_Name"
F 9 "0751.0062" H 2000 1950 50  0001 L CNN "Manufacturer_Part_Number"
	1    1350 2550
	1    0    0    -1  
$EndComp
Connection ~ 1350 2650
$Comp
L SamacSys_Parts:0751.0062 F102
U 1 1 60B83BAC
P 1350 3000
F 0 "F102" V 1700 2950 50  0000 L CNN
F 1 "0751.0062" V 1800 2800 50  0000 L CNN
F 2 "07510062" H 2000 3100 50  0001 L CNN
F 3 "https://us.schurter.com/en/datasheet/typ_OG__Clip__5x20.pdf" H 2000 3000 50  0001 L CNN
F 4 "Fuse Clip 6.3A 500VDC/500VAC Solder Pin Through Hole Bulk" H 2000 2900 50  0001 L CNN "Description"
F 5 "12" H 2000 2800 50  0001 L CNN "Height"
F 6 "693-0751.0062" H 2000 2700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Schurter/07510062?qs=WtG364jHAdwzmGym3exHtQ%3D%3D" H 2000 2600 50  0001 L CNN "Mouser Price/Stock"
F 8 "SCHURTER" H 2000 2500 50  0001 L CNN "Manufacturer_Name"
F 9 "0751.0062" H 2000 2400 50  0001 L CNN "Manufacturer_Part_Number"
	1    1350 3000
	1    0    0    -1  
$EndComp
Connection ~ 1350 3100
Wire Wire Line
	1350 3100 1350 3350
$EndSCHEMATC
